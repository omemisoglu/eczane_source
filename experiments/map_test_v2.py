# -*- coding: utf-8 -*-
import os
import numpy as np
import time
from SSIM_PIL import compare_ssim
from PIL import Image
import imagehash
import itertools
from tqdm import tqdm
import datetime
import pandas
from sklearn.preprocessing import StandardScaler
import scipy.signal
import dicttoxml
from xml.dom.minidom import parseString
import math

import tr_string as trstr
import webdrive as wd


os.chdir(r'C:\Users\omemi\workspace\eczane_source')


DRIVER_PATH = 'drivers/win10/chromedriver.exe'
opts = ["--disable-infobars"]

def download_trainset(w=1250, h=840):
    service = wd.Service(DRIVER_PATH)
    service.start()
    webdriver = wd.Webdriver(service, DRIVER_PATH, options=opts)
    webdriver.driver.set_window_size(w, h)
    webdriver.driver.set_window_position(-3000, -2000)
    
    dir_file = 'database'
    t_dir_img = os.path.abspath(os.path.join('.temp','map_tmp.png'))

#    city_nr_list = {'22': 'Gürkan Eczanesi,Merkez',
#                    '39': 'Şirin Eczanesi,Lüleburgaz'}
    city_nr_list = ['22', '39']
    for i in city_nr_list:
        
        excel_path = os.path.join(dir_file, i + '_eczaneler.xlsx')
        duty_list = pandas.read_excel(excel_path)
        print(city_nr_list.index(i), '/', '2')
        for k, rowi in duty_list.iterrows():
            duty_list_temp = duty_list.loc[duty_list.iloc[:,1] == rowi[1]]
            PHAR_NAME = rowi[0]
            str_phar = trstr.TR_to_EN(rowi[0]                            \
                                               .replace(' Eczanesi', '')        \
                                               .replace(' ', ''),               \
                                           'lower')
            PHAR_DIST = rowi[1]
            PHAR_GEOLOC = rowi[4]
            print('\r\t', str(k) + '/' + str(len(duty_list)))
            for j, row in duty_list_temp.iterrows():
                if not((row[0] == PHAR_NAME) or (row[1] != PHAR_DIST)):
                    print('\r\t\t', str(j) + '/' + str(len(duty_list_temp)))
                    str_1 = trstr.TR_to_EN(row[1].replace(' ',''),'lower')
                    str_2 = trstr.TR_to_EN(row[0]                            \
                                               .replace(' Eczanesi', '')        \
                                               .replace(' ', ''),               \
                                           'lower')
                    map_db_name = i + str_1 + '_' + str_phar +'-' + str_2 + '.png'
                    org = PHAR_GEOLOC
                    dest = row[4]
                    
                    for ii in range(0,2):
                        if ii:
                            dir_sign = 'map_base'
                        else:
                            dir_sign = 'map_test'
                        
                        dir_out = os.path.join('.temp', dir_sign)
                        map_db_path = os.path.abspath(os.path.join(dir_out, map_db_name))
                        
                       
                        if ii:
#                            webdriver.open('https://www.openstreetmap.org/directions?engine=\
#                                    fossgis_osrm_car&route=' + org + ';' + dest)
                            time.sleep(6)
                            map_loaded = False
                            t_count = 0
                            while not(map_loaded) and t_count < 3:
                                class_list = ['leaflet-tile-pane',
                                              'leaflet-shadow-pane',
                                              'leaflet-overlay-pane']
                                map_loaded = webdriver.check_condition(class_list)
                                t_count += 1
                            webdriver.screenshot(t_dir_img)
                        else:
                            webdriver.open('https://www.openstreetmap.org/directions?engine=\
                               fossgis_osrm_car&route=' + org + ';' + dest)
                            webdriver.screenshot(t_dir_img)
                        
                        img = Image.open(t_dir_img)
                        width, height = img.size
                        right = width - 50
                        bottom = height - 22
                        left= right - 880 if width > 880 else 0
                        top = bottom - 660 if height > 660 else 0
                
                        img_map = img.crop((left, top, right, bottom))
                        
                        # Add OSM Banner
                        #
                        banner = Image.open(os.path.join('html', 'resources',
                                                         'osm-banner.png'))
                        palette = Image.new('RGBA', img_map.size)
                        palette.paste(banner, (img_map.width - banner.width, \
                                               img_map.height - banner.height), 
                                      mask=banner)
                        img_out = Image.alpha_composite(img_map, palette)
                        img_out = img_out.convert('RGB')
                        
                        # Save image
                        img_out.save(map_db_path, format='JPEG')
                        print(map_db_path.split('\\')[-1], 'saved')
                        os.remove(t_dir_img)
    webdriver.driver.quit()
    service.stop()

def mse(imageA, imageB):
#    imA = imageA.astype("float")
#    imB = imageB.astype("float")
    
    imA = np.array(imageA).astype('float')
    imB = np.array(imageB).astype('float')
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((imA - imB) ** 2)
    err /= float(imA.shape[0] * imA.shape[1])
    
    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err

def cross_image(im1, im2):
    # get rid of the color channels by performing a grayscale transform
    # the type cast into 'float' is to avoid overflows
    im1_gray = np.sum(np.array(im1).astype('float'), axis=2)
    im2_gray = np.sum(np.array(im2).astype('float'), axis=2)

    # get rid of the averages, otherwise the results are not good
    im1_gray -= np.mean(im1_gray)
    im2_gray -= np.mean(im2_gray)

    # calculate the correlation image; note the flipping of onw of the images
    return scipy.signal.fftconvolve(im1_gray, im2_gray[::-1,::-1], mode='same')

def get_corners(width, height, square_size, w_cur=0, h_cur=0, w_step=None, h_step=None):
    ans_list = []
    very_right = False
    very_bottom = False
    if w_step == None or h_step == None:
        if isinstance(square_size, tuple):
            w_step = width // square_size[0]
            h_step = height // square_size[1]
        else:
            w_step = width // square_size
            h_step = height // square_size
    
    width_next = w_cur + (2 * w_step)
    if width_next > width and abs(width_next - width) < w_step:
        w_step = width - w_cur
        
    height_next = h_cur + (2 * h_step)
    if height_next > height and abs(height_next - height) < h_step:
        h_step = height - h_cur
    
    if (w_cur + w_step >= width):
        very_right = True
    if (h_cur + h_step >= height):
        very_bottom = True

    if not(very_bottom) and w_cur == 0:
        dot_bottom = get_corners(width, height, square_size,
                            w_cur, h_cur + h_step, w_step, h_step)
        if (dot_bottom != None and isinstance(dot_bottom, list)):
            ans_list += dot_bottom

    if not(very_right):
        dot_right = get_corners(width, height, square_size,
                                w_cur + w_step, h_cur, w_step, h_step)
        if (dot_right != None and isinstance(dot_right, list)):
            ans_list += dot_right

    ans_list.append((w_cur, h_cur, w_cur + w_step, h_cur + h_step))
    if not(w_cur + h_cur):
        ans_list.reverse()
    return ans_list

def is_similar(img_a, img_b):
    '''
    Calculates 
    
    # Imports:
        import math
        (custom) import 
    '''
    ans = False
    
    ### Hash Difference
    #
    hash_first = imagehash.average_hash(img_a)
    hash_second = imagehash.average_hash(img_b)
    hash_main = abs(hash_first - hash_second)
    
    ### Sub Image Hash Differences
    
    # Sub Image Bounding Boxes
    rect_list = get_corners(img_a.size[0], img_a.size[1], square_size=7)
    
    hash_sub_list = []
    for rect in rect_list:
        # Cropping sub images
        img_a_sub = img_a.crop(rect)
        img_b_sub = img_b.crop(rect)
        
        # Hash Differences
        hash_first = imagehash.average_hash(img_a_sub)
        hash_second = imagehash.average_hash(img_b_sub)
        hash_sub = abs(hash_first - hash_second)
        hash_sub_list.append([hash_sub, hash_sub])
        
    ### Standart Scale with Mean
    #
    scaler = StandardScaler()
    scaler.fit(hash_sub_list)
    mean = round(scaler.mean_[0], 2)
    scl = round(scaler.scale_[0], 2)
    
    ### Cross Correlation
    #
    corr_img = cross_image(img_a, img_b)
    corr_point = np.unravel_index(np.argmax(corr_img), corr_img.shape)
    # Translation Vector
    corr_vector = ((img_a.size[0] / 2) - corr_point[1],
                   (img_a.size[1] / 2) - corr_point[0])
    # Length of the correlation error
    corr_main = round(math.hypot(corr_vector[0], corr_vector[1]), 2)

    ### Combined Filter
    #
    if hash_main <= 15 and ((scl <= 2.0 and mean <= 10.0) or 
                            (0.0 < corr_main and corr_main < 20.0 and hash_main < 5)):
        ans = True
    
    return ans

# Test
def calc_sim_v2(inp_list, path_out):

    dict_test = {}

    for i in tqdm(inp_list):
        img_base_path =  i[0]
        image_base = Image.open(img_base_path)
        img_test_path = i[1]
        image_test = Image.open(img_test_path)
        
        if is_similar(image_base, image_test):
            # Test
            dict_key = 'A' +  i[0].split('\\')[-1]
            dict_test[dict_key] = {'True': 1}
    
    return dict_test

def calc_sim(inp_list, path_out):
    f = open(path_out, 'w')
    my_dict = {}
    img_init = Image.open(inp_list[0][0])
    width = img_init.size[0]
    height = img_init.size[1]
    rect_list = get_corners(width, height, square_size=7)

    for i in tqdm(inp_list):
        dict_key = 'A' +  i[0].split('\\')[-1]
        my_dict[dict_key] = {}
        
        img_base_path =  i[0]
        image_base = Image.open(img_base_path)
        img_test_path = i[1]
        image_test = Image.open(img_test_path)
        
        # Calculate general hash
        #
        hash_base = imagehash.average_hash(image_base)
        hash_test = imagehash.average_hash(image_test)
        val_hash = abs(hash_base - hash_test)
        
        # Cross correlation
        #
        corr_img = cross_image(image_base, image_test)
        corr_point = np.unravel_index(np.argmax(corr_img), corr_img.shape)
        corr_dist = ((width / 2) - corr_point[1], (height / 2) - corr_point[0])
        
        my_dict[dict_key]['corr'] = corr_dist
        my_dict[dict_key]['hash'] = val_hash
        my_dict[dict_key]['mean'] = ''
        my_dict[dict_key]['scale'] = ''
        
        my_dict[dict_key]['sub_hash'] = {}
        sub_dict = my_dict[dict_key]['sub_hash']
        val_tot = 0
        val_list = []
        for rect in rect_list:
            t_idx = rect_list.index(rect) + 1

            im_ba = image_base.crop(rect)
            im_te = image_test.crop(rect)
            
            hash_base = imagehash.average_hash(im_ba)
            hash_test = imagehash.average_hash(im_te)
            val_hash = abs(hash_base - hash_test)
            val_tot += val_hash
            val_list.append([val_hash, val_hash])
            point = '{idx:02d} - ({w0},{h0}) ({w1},{h1})'.format(idx=t_idx,
                     w0=rect[0], h0=rect[1], w1=rect[2], h1=rect[3])
            sub_dict[point] = val_hash
        scaler = StandardScaler()
        scaler.fit(val_list)
        mean = scaler.mean_
        scl = scaler.scale_
        my_dict[dict_key]['mean'] = round(mean[0], 2)
        my_dict[dict_key]['scale'] = round(scl[0], 2)

#        val_ssim = compare_ssim(image_base.convert('LA'), image_test.convert('LA'))
#        val_mse = mse(image_base.convert('LA'), image_test.convert('LA'))
#        my_dict[dict_key]['ssim'] = val_ssim
#        my_dict[dict_key]['mse'] = val_mse
#        
#        f.write('\tssim = {ss}'.format(ss=val_ssim))
#        f.write('\tmse = {mse}'.format(mse=val_mse))
    
    f.write(parseString(dicttoxml.dicttoxml(my_dict, ids=False)).toprettyxml())
    f.close()
    
    return my_dict

def get_today_string():
    today = datetime.datetime.today()
    today_str = today.date().isoformat()
    today_time =  today.strftime('%H.%M')
    today_full_str = today_str + '_' +today_time
    return today_full_str

# Get time and date now as string <date in iso format>_<time format: HH.MM>
#
today_full_str = get_today_string()

dir_base_list = os.listdir('.temp\\map_base')
dir_test_list = os.listdir('.temp\\map_test')
dir_list = dir_base_list if len(dir_base_list) <= len(dir_test_list) else dir_test_list
list_match = [(os.path.join('.temp\\map_base', i), os.path.join('.temp\\map_test', i)) for i in dir_list]

return_dict = calc_sim(list_match, os.path.join('.temp', today_full_str + '.xml'))

#f = open(os.path.join('.temp','res_ss.xml'), 'w')
#for key, val in return_dict.items():
#    t_list = []
#    f.write('<{key}>\n'.format(key=key))
#    for t_key, t_val in val.items():
#        f.write('\tHash #{k}: {t_val}\n'.format(k=t_key, t_val=t_val))
#        t_list.append([t_val, t_val])
#    scaler = StandardScaler()
#    scaler.fit(t_list)
#    mean = scaler.mean_
#    scl = scaler.scale_
#    val['mean'] = mean
#    val['scl'] = scl
#    f.write('\tMean: {mean:.2f}\n\tStd: {scl:.3f}\n</{key}>\n'
#            .format(key=key, mean=mean[0], scl=scl[0]))
#f.close()

dict_filtered = {}
for key, val in return_dict.items():
    dist = round(math.hypot(val['corr'][0], val['corr'][1]), 2)
#    avg_dif = abs(val['hash'] - val['mean'])
    if val['hash'] <= 15 and (
            (val['scale'] <= 2.0 and val['mean'] <= 10.0) or 
            (0.0 < dist and dist < 20.0 and val['hash'] < 5)
        ):
        dict_filtered[key] = val
        dir_open = os.path.join('.temp\\map_test', key)

dict_filtered_v2 = calc_sim_v2(list_match, os.path.join('.temp', today_full_str + '.xml'))

if list(dict_filtered.keys()) == list(dict_filtered_v2.keys()):
    print('Function Matches!!!')
else:
    print(':((((((')
##########

if list(dict_filtered.keys()) == t_1:
    print('Function Matches!!!')
else:
    print(':((((((')
##########

for i in list_match:
    i_base = Image.open(i[0])
    i_test = Image.open(i[1])

    break

