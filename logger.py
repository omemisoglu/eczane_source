# -*- coding: utf-8 -*-

import os
from datetime import datetime
import traceback

from file_manager import Clean_Folder
# Logs the exception with traceback in a log folder of the changing directory)
#
# @param msg[in]    : Error to be logged. Default is the traceback 
#                     from exception
# @param dir_log[in]: Log directory relative to the function's start directory
#
# @return           : Absolute path to the log file
#
def Exception_Log(dir_log, msg=None):
    dt_obj = datetime.now()
    log_file = dt_obj.strftime('ErrorLog_%Y-%m-%d__%H.%M.%S.log')
    
    if len(os.listdir(dir_log)) > 100:
        Clean_Folder(dir_log, 50)
    
    out = ''
    out += 'Date: ' + str(dt_obj.date()) + '\n'
    out += 'Time: ' + str(dt_obj.time()) + '\n'
    out += 'Date-time: ' + str(dt_obj) + '\n\n'
    
    if (msg == None):
        out += str(traceback.format_exc())
    else:
        out += msg
    
    cur_cwd = os.getcwd()
    os.chdir(dir_log)
    
    log_cwd = os.getcwd()
    logf = open(log_file, 'w')
    logf.write(out)
    logf.close()
    
    os.chdir(cur_cwd)
    return log_cwd + '/' + log_file

def Dict_to_XML(inp_dict, path, ids=False):
    import dicttoxml
    from xml.dom.minidom import parseString
    
    f = open(path, 'w')
    f.write(parseString(dicttoxml.dicttoxml(inp_dict, ids=ids)).toprettyxml())
    f.close()