function GetClock(){
    var dayName = new Array("Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", 
                            "Cuma", "Cumartesi");
    var today = new Date();
    var dd    = String(today.getDate()).padStart(2, "0"),
        mm    = String(today.getMonth() + 1).padStart(2, "0"), //January is 0!
        yyyy  = today.getFullYear(),
        hh    = String(today.getHours()).padStart(2, "0"),
        dmin  = String(today.getMinutes()).padStart(2, "0"),
        dsec  = String(today.getSeconds()).padStart(2, "0"),
        d_name = dayName[today.getDay()];
        
    dtDate = dd + "." + mm + "." + yyyy + " " + d_name +"\xa0\xa0\xa0";
    dtClock = hh + ":" + dmin + ":" + dsec;
    document.getElementById("datebox").innerHTML = dtDate;
    document.getElementById("clockbox").innerHTML = dtClock;
    
    Adjust_Screen();
}
function Adjust_Screen(){
    document.body.style.height = "100%";
    document.body.style.weight = "100%";
}
// window.onload=function(){
    // GetClock();
    // Adjust_Screen();
    // setInterval(GetClock,1000);
// }


