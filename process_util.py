# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 14:30:42 2019

@author: omemi
"""

import signal, psutil

def get_process(pid):
    try:
        parent = psutil.Process(pid)
    except psutil.NoSuchProcess:
        return None
    
    return parent

def get_children(parent_pid, recursive=True):
    parent = get_process(parent_pid)
    
    if parent == None:
        return None
    
    children = parent.children(recursive=recursive)
    
    return children

def kill_children(parent_pid, kill_me=True, recursive=True, sig=signal.SIGTERM):
    
    parent = get_process(parent_pid)
    if parent == None:
        return
    
    children = parent.children(recursive=recursive)
    
    for process in children:
        process.send_signal(sig)
    
    if kill_me:
        parent.send_signal(sig)

#    try:
#        parent = psutil.Process(pid)
#    except psutil.NoSuchProcess:
#        return None