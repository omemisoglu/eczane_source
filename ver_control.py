# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 17:53:57 2019

@author: omemi
"""
import subprocess
import os
import sys
import re
import time
import shlex
import pygit2

from scripter import Sh_Script_Write
import logger as lg

def Debug_Print(msg, DEBUG):
    if DEBUG:
        print(msg)

def Version_Check(DIR_LOG='.log', DEBUG=False):
    ans = False
    # Get the current branch
    #
#    result = subprocess.run(['git', 'rev-parse', '--abbrev-ref', 'HEAD'],   \
#                        stdout=subprocess.PIPE)
#    curr_branch = result.stdout.decode().strip()
    repo = pygit2.Repository(pygit2.discover_repository(os.getcwd()))
    curr_branch = repo.head.shorthand
    
    Debug_Print("Curr Branch: " + curr_branch, DEBUG)
    
    ver_base = ''
    if (curr_branch == 'master'):
        ver_base = 'r'
    else:
        ver_base = 'd'
#    org_branch = 'origin/' + curr_branch
    
    # Get the current version
    #
#    result = subprocess.run(['git', 'tag', '--list', '--sort=-taggerdate', \
#                             '--points-at='+org_branch],                 \
#                            stdout=subprocess.PIPE)
#    result = subprocess.run(['git', 'describe', '--tags','--abbrev=0'],      \
#                            stdout=subprocess.PIPE)
#    
#    git_out = result.stdout.decode()
    
    git_out = repo.describe(abbreviated_size=0)
    ver_current = re.findall(r'\d+.\d+', git_out)
    
#    ptrn_tmpl = '(?<=^refs/tags/)([a-zA-Z0-9_/.,]+)'# (\w+)
#    ptrn = re.compile(ptrn_tmpl)
#    tags = [ptrn.findall(ref)[0] for ref in repo.references if len(ptrn.findall(ref))]
#    tags.reverse()
    
#    for remote in repo.remotes:
#        print(remote.name)
#        if remote.name == 'origin':
#            remote.fetch()
    
    t_pass = False
    while not(t_pass):
        try:
            # Fetch tags if new version tag is available
            #
            result = subprocess.run(['git', 'fetch', 'origin', curr_branch, '--tags'],
                                stdout=subprocess.PIPE, timeout=60, check=True)
            t_pass = True
        except:
            pass
    
    # Get the last version
    #
    result = subprocess.run(['git', 'tag', '--list', '--sort=-taggerdate'],   \
                            stdout=subprocess.PIPE)
    git_out = result.stdout.decode()
    ver_last = re.findall(ver_base + r'\d+.\d+', git_out)
    
    if len(ver_current):
        Debug_Print("Curr Version: " + ver_current[0], DEBUG)
    if len(ver_last):
        Debug_Print("Remote Version: " + ver_last[0], DEBUG)
    
    if (not(len(ver_last) and len(ver_current))):
        if not(DEBUG):
            raise Exception('No version tag found!!!')
    else:
        # If version is higher than the current, return 'True' to update
        #
        if (float(ver_last[0].replace(ver_base, '')) > float(ver_current[0])):
            ans = True
            Debug_Print("New Version available!", DEBUG)
    
    return ans
    
def Version_Update(cwdir, prg_exe, DEBUG=False):
    """
    cwdir:
    """
    repo = pygit2.Repository(pygit2.discover_repository(os.getcwd()))
    curr_branch = repo.head.shorthand
    
#    os.chdir('../')
    
    # Write updater python script with arguments
    #
    scr_path = os.path.abspath(os.path.join('..','update.py'))
    scr_lines = [
        "# -*- coding: utf-8 -*-",
        "import subprocess",
        "import pygit2",
        "import shlex",
        "import time",
        "import os",
        "import sys",
        "args_exe = " + '"' + prg_exe + '"',
        "os.chdir('" + cwdir + "')",
        "t_failure = True",
        "time.sleep(2)",
        "while t_failure:",
        "    try:",
        "        subprocess.run(shlex.split('git pull origin " + curr_branch + "'), \\",
        "            timeout=60, check=True)",
        "        t_failure = False",
        "    except:",
        "        time.sleep(5)",
        "        pass",
        "time.sleep(3)",
        "with open(os.devnull, 'r+b', 0) as DEVNULL:",
        "    subprocess.Popen(shlex.split(args_exe),    \\",
        "        stdin=DEVNULL, stdout=DEVNULL,         \\",
        "        stderr=subprocess.STDOUT,              \\",
        "        close_fds=True)",
        "time.sleep(3)",
        'subprocess.Popen(' + r'"python3 -c \"import os, time; time.sleep(1);' +
        r"os.remove('{}')"+ r';\"".format(sys.argv[0]), shell=True)',
        "sys.exit(0)"
        ]
    upt_exe = Sh_Script_Write(scr_path, scr_lines)
    
    with open(os.devnull, 'r+b', 0) as DEVNULL:
        subprocess.Popen(shlex.split(upt_exe),    \
                         stdin=DEVNULL, stdout=DEVNULL,  \
                         stderr=subprocess.STDOUT,       \
                         close_fds=True)
    time.sleep(2)
    sys.exit()

def pygit2_pull(repo, remote_name='origin', branch='master'):
    for remote in repo.remotes:
        if remote.name == remote_name:
            remote.fetch()
            remote_master_id = repo.lookup_reference('refs/remotes/origin/%s' % (branch)).target
            merge_result, _ = repo.merge_analysis(remote_master_id)
            # Up to date, do nothing
            if merge_result & pygit2.GIT_MERGE_ANALYSIS_UP_TO_DATE:
                return
            # We can just fastforward
            elif merge_result & pygit2.GIT_MERGE_ANALYSIS_FASTFORWARD:
                repo.checkout_tree(repo.get(remote_master_id))
                try:
                    master_ref = repo.lookup_reference('refs/heads/%s' % (branch))
                    master_ref.set_target(remote_master_id)
                except KeyError:
                    repo.create_branch(branch, repo.get(remote_master_id))
                repo.head.set_target(remote_master_id)
            elif merge_result & pygit2.GIT_MERGE_ANALYSIS_NORMAL:
                repo.merge(remote_master_id)

                if repo.index.conflicts is not None:
                    for conflict in repo.index.conflicts:
                        print('Conflicts found in:', conflict[0].path)
                    raise AssertionError('Conflicts, ahhhhh!!')

                user = repo.default_signature
                tree = repo.index.write_tree()
                commit = repo.create_commit('HEAD',
                                            user,
                                            user,
                                            'Merge!',
                                            tree,
                                            [repo.head.target, remote_master_id])
                # We need to do this or git CLI will think we are still merging.
                repo.state_cleanup()
            else:
                raise AssertionError('Unknown merge analysis result')


def pygit2_push(repo, remote_name='origin', ref='refs/heads/master:refs/heads/master'):
    for remote in repo.remotes:
        if remote.name == remote_name:
            remote.push(ref)
